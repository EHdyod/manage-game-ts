import Slot from "@/store/types/Slot";
import Seal from "@/store/types/Seal";

export default interface Tool {
    name: string;
    nbEmbedded: number;
    nbMaxEmbedded: number;
    slots: Slot[];

    getName(): string;
    getNbEmbedded(): number;
    getNbMaxEmbedded(): number;
    insertSealInSlot(seal:Seal, index: number): void;
    removeSealFromSlot(index: number): void;
}