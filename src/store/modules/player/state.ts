import Seal from "@/store/types/Seal";
export type State = {
    name: string;
    energy: number;
    seals: Seal[];
}

export const state: State = {
    name: "",
    energy: 0,
    seals: new Array<Seal>()
};