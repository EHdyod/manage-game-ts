import Slot from "@/store/types/Slot";

export type State = {
    name: string,
    nbEmbedded: number,
    nbMaxEmbedded: number,
    slots: Slot[]
    totalEnergy: number
}

const nbSlots = 5;

export const state: State = {
    name: "Astral Pentagram",
    nbEmbedded: 0,
    nbMaxEmbedded: nbSlots,
    slots: [new Slot(2),new Slot(2),new Slot(2),new Slot(2),new Slot(2)],
    totalEnergy: 0
};